package com.example.javaetl;

import com.example.javaetl.tasks.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class Processor {
    private static final Map<String, String> fileMapping = new HashMap<>();

    static {
        fileMapping.put("src/main/resources/data/integrationA/integration16042019.csv", "src/main/resources/mapping/integrationA.json");
        fileMapping.put("src/main/resources/data/payrolls/sponsor/1_test_04022019.csv", "src/main/resources/mapping/sponsor_1.json");
        fileMapping.put("src/main/resources/data/payrolls/sponsor/2_s_21022019.csv", "src/main/resources/mapping/sponsor_2.json");
    }

    private static final String dictionary = "src/main/resources/mapping/dictionary.json";

    static void walk() {
        try {
            Files.walk(Paths.get("src/main/resources/data"))
                    .filter(Files::isRegularFile)
                    .forEach(path -> run(path.toString(), fileMapping.get(path.toString())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void run(final String data, final String mapping) {
        final TaskMetadata taskMetadata = getTaskMetadata(data, mapping);

        List<String> retrieve = null;
        List<Map<String, String>> transform = null;
        List<Map<String, String>> normalize = null;
        List<Map<String, String>> validate = null;
        List<BaseModel> load = null;

        final long start = System.nanoTime();
        final Step step = Step.START;
        switch (step) {
            case START:
                System.out.println("started processing file: " + taskMetadata.getDataFileName());
            case RETRIEVE:
                final RetrieveTask retrieveTask = new RetrieveTask();
                retrieve = retrieveTask.execute(data);
            case TRANSFORM:
                final TransformTask transformTask = new TransformTask(taskMetadata);
                transform = transformTask.execute(retrieve);
            case NORMALIZE:
                final NormalizeTask normalizeTask = new NormalizeTask(taskMetadata);
                normalize = normalizeTask.execute(transform);
            case VALIDATE:
                final ValidateTask validateTask = new ValidateTask(taskMetadata);
                validate = validateTask.execute(normalize);
            case LOAD:
                final LoadTask loadTask = new LoadTask(taskMetadata);
                load = loadTask.execute(validate);
            case FINISH:
                assert load != null;
                load.forEach(System.out::println);
                System.out.println("finished after " + (System.nanoTime() - start) / 1_000_000);
        }
    }

    private static TaskMetadata getTaskMetadata(final String data, final String mapping) {
        final ObjectMapper objectMapper = new ObjectMapper();
        try {
            final String dataFileName = Paths.get(data).getFileName().toString();
            final Map headers = objectMapper.readValue(new File(mapping), LinkedHashMap.class);
            final Map dictionary = objectMapper.readValue(new File(Processor.dictionary), LinkedHashMap.class);
            return new TaskMetadata(dataFileName, headers, dictionary);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't create task metadata for the file: " + data, e);
        }
    }

}
