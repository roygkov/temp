package com.example.javaetl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TaskMetadata {

    private final String dataFileName;
    private final Map<String, String> headers;
    private final Map<String, String> dictionary;

    private final List<String> requiredHeaders = Arrays.asList("PLAN_ID", "DIVISION", "PAY_DATE");

    TaskMetadata(final String dataFileName, final Map<String, String> headers, final Map<String, String> dictionary) {
        this.dataFileName = dataFileName;
        this.headers = headers;
        this.dictionary = dictionary;
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getDictionary() {
        return dictionary;
    }

    public List<String> getRequiredHeaders() {
        return requiredHeaders;
    }

}
