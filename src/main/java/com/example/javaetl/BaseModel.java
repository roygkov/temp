package com.example.javaetl;

public class BaseModel {

    private String dateOfBirth;
    private String division;
    private String emAmount;
    private String firstName;
    private String lastName;
    private String lnAmount;
    private String payDate;
    private String planId;
    private String preTaxAmount;
    private String psAmount;
    private String qcAmount;
    private String sfAmount;
    private String ssn;
    private String workingHours;

    public void setDateOfBirth(final String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setDivision(final String division) {
        this.division = division;
    }

    public void setEmAmount(final String emAmount) {
        this.emAmount = emAmount;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public void setLnAmount(final String lnAmount) {
        this.lnAmount = lnAmount;
    }

    public void setPayDate(final String payDate) {
        this.payDate = payDate;
    }

    public void setPlanId(final String planId) {
        this.planId = planId;
    }

    public void setPreTaxAmount(final String preTaxAmount) {
        this.preTaxAmount = preTaxAmount;
    }

    public void setPsAmount(final String psAmount) {
        this.psAmount = psAmount;
    }

    public void setQcAmount(final String qcAmount) {
        this.qcAmount = qcAmount;
    }

    public void setSfAmount(final String sfAmount) {
        this.sfAmount = sfAmount;
    }

    public void setSsn(final String ssn) {
        this.ssn = ssn;
    }

    public void setWorkingHours(final String workingHours) {
        this.workingHours = workingHours;
    }

    @Override
    public String toString() {
        return "BaseModel{" +
                "dateOfBirth='" + dateOfBirth + '\'' +
                ", division='" + division + '\'' +
                ", emAmount='" + emAmount + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", lnAmount='" + lnAmount + '\'' +
                ", payDate='" + payDate + '\'' +
                ", planId='" + planId + '\'' +
                ", preTaxAmount='" + preTaxAmount + '\'' +
                ", psAmount='" + psAmount + '\'' +
                ", qcAmount='" + qcAmount + '\'' +
                ", sfAmount='" + sfAmount + '\'' +
                ", ssn='" + ssn + '\'' +
                ", workingHours='" + workingHours + '\'' +
                '}';
    }

}
