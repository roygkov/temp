package com.example.javaetl.tasks;

import com.example.javaetl.TaskMetadata;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TransformTask implements TaskExecutor<List<String>, List<Map<String, String>>> {

    private final TaskMetadata taskMetadata;

    public TransformTask(final TaskMetadata taskMetadata) {
        this.taskMetadata = taskMetadata;
    }

    @Override
    public List<Map<String, String>> execute(final List<String> lines) {
        final ArrayList<String> headers = new ArrayList<>(Arrays.asList(lines.get(0).split(",")));

        List<String> strings;

        if (headers.containsAll(taskMetadata.getRequiredHeaders())) {
            strings = lines.stream().skip(1).collect(Collectors.toList());
        } else {
            updateHeaders(headers);
            strings = updateLines(lines);
        }

        return strings.stream()
                .map(line -> line.split(","))
                .map(data -> IntStream.range(0, data.length)
                        .boxed()
                        .collect(Collectors.toMap(headers::get, i -> data[i])))
                .collect(Collectors.toList());
    }

    private Optional<String> findByRegex(final String dataFileName, final String regex) {
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(dataFileName);
        if (matcher.find()) {
            return Optional.of(matcher.group(1));
        }
        return Optional.empty();
    }

    private void updateHeaders(ArrayList<String> headers) {
        headers.add(0, taskMetadata.getRequiredHeaders().get(0));
        headers.add(1, taskMetadata.getRequiredHeaders().get(1));
        headers.add(2, taskMetadata.getRequiredHeaders().get(2));
    }

    private List<String> updateLines(final List<String> lines) {
        final String dataFileName = taskMetadata.getDataFileName();
        final Optional<String> planId = findByRegex(dataFileName, "^(\\d*)");
        final Optional<String> division = findByRegex(dataFileName, "_+(\\w+?)_");
        final Optional<String> payDate = findByRegex(dataFileName, "_+(\\d*)\\.");

        if (!planId.isPresent() || !division.isPresent() || !payDate.isPresent()) {
            throw new RuntimeException("Transform failed due to missing required headers data in file " + dataFileName);
        }

        return lines.stream()
                .skip(1)
                .map(s -> planId.get() + "," + division.get() + "," + payDate.get() + "," + s)
                .collect(Collectors.toList());
    }

}
