package com.example.javaetl.tasks;

@FunctionalInterface
public interface TaskExecutor<IN, OUT> {

    OUT execute(IN in);

}
