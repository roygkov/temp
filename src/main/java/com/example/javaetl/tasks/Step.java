package com.example.javaetl.tasks;

public enum Step {
    START,
    RETRIEVE,
    TRANSFORM,
    NORMALIZE,
    VALIDATE,
    LOAD,
    FINISH
}
