package com.example.javaetl.tasks;

import com.example.javaetl.TaskMetadata;

import java.util.List;
import java.util.Map;

public class ValidateTask implements TaskExecutor<List<Map<String, String>>, List<Map<String, String>>> {

    private final TaskMetadata taskMetadata;

    public ValidateTask(final TaskMetadata taskMetadata) {
        this.taskMetadata = taskMetadata;
    }

    @Override
    public List<Map<String, String>> execute(final List<Map<String, String>> maps) {
        maps.removeIf(map -> map.get(taskMetadata.getHeaders().get("planId")).equals("")
                || map.get(taskMetadata.getHeaders().get("division")).equals(""));

        return maps;
    }

}
