package com.example.javaetl.tasks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class RetrieveTask implements TaskExecutor<String, List<String>> {

    @Override
    public List<String> execute(final String path) {
        try {
            return Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read file: " + path, e);
        }
    }

}
