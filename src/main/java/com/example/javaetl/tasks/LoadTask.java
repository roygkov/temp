package com.example.javaetl.tasks;

import com.example.javaetl.BaseModel;
import com.example.javaetl.TaskMetadata;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LoadTask implements TaskExecutor<List<Map<String, String>>, List<BaseModel>> {

    private final TaskMetadata taskMetadata;

    public LoadTask(final TaskMetadata taskMetadata) {
        this.taskMetadata = taskMetadata;
    }

    @Override
    public List<BaseModel> execute(final List<Map<String, String>> maps) {
        return maps.stream()
                .map(map -> {
                    final BaseModel baseModel = new BaseModel();

                    baseModel.setDateOfBirth(map.get(taskMetadata.getHeaders().get("dateOfBirth")));
                    baseModel.setDivision(map.get(taskMetadata.getHeaders().get("division")));
                    baseModel.setEmAmount(map.get(taskMetadata.getHeaders().get("emAmount")));
                    baseModel.setFirstName(map.get(taskMetadata.getHeaders().get("firstName")));
                    baseModel.setLastName(map.get(taskMetadata.getHeaders().get("lastName")));
                    baseModel.setLnAmount(map.get(taskMetadata.getHeaders().get("lnAmount")));
                    baseModel.setPayDate(map.get(taskMetadata.getHeaders().get("payDate")));
                    baseModel.setPlanId(map.get(taskMetadata.getHeaders().get("planId")));
                    baseModel.setPreTaxAmount(map.get(taskMetadata.getHeaders().get("preTaxAmount")));
                    baseModel.setPsAmount(map.get(taskMetadata.getHeaders().get("psAmount")));
                    baseModel.setQcAmount(map.get(taskMetadata.getHeaders().get("qcAmount")));
                    baseModel.setSfAmount(map.get(taskMetadata.getHeaders().get("sfAmount")));
                    baseModel.setSsn(map.get(taskMetadata.getHeaders().get("ssn")));
                    baseModel.setWorkingHours(map.get(taskMetadata.getHeaders().get("workingHours")));

                    return baseModel;
                })
                .collect(Collectors.toList());
    }

}
