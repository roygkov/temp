package com.example.javaetl.tasks;

import com.example.javaetl.TaskMetadata;

import java.util.List;
import java.util.Map;

public class NormalizeTask implements TaskExecutor<List<Map<String, String>>, List<Map<String, String>>> {

    private final TaskMetadata taskMetadata;

    public NormalizeTask(final TaskMetadata taskMetadata) {
        this.taskMetadata = taskMetadata;
    }

    @Override
    public List<Map<String, String>> execute(final List<Map<String, String>> maps) {
        maps.forEach(map -> map.entrySet().stream()
                .peek(stringEntry -> stringEntry.setValue(stringEntry.getValue().trim().replaceAll("\t", "")))
                .filter(entry -> taskMetadata.getDictionary().containsKey(entry.getValue()))
                .forEach(entry -> entry.setValue(taskMetadata.getDictionary().get(entry.getValue()))));

        return maps;
    }

}
